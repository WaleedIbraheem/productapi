﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Products.API.Models;
using Products.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : ControllerBase
    {
        private readonly IProductLibraryRepository _context;
        private readonly IMapper _mapper;

        public CategoriesController(IProductLibraryRepository context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public ActionResult<IEnumerable<CategoryResultDto>> GetCategories()
        {
            var categoriesFromRepo = _context.GetCategories();
            var categoryDto = _mapper.Map<IEnumerable<CategoryResultDto>>(categoriesFromRepo);
            return StatusCode(StatusCodes.Status200OK, new { success = true, results = categoryDto });
        }
    }
}
