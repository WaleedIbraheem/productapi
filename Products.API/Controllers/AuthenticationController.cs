﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Products.API.Entities;
using Products.API.Models;
using Products.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly IProductLibraryRepository _context;
        private readonly IMapper _mapper;

        public AuthenticationController(IProductLibraryRepository context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpPost("register")]
        public ActionResult<string> Register([FromBody] RegisterDto user)
        {
            var userEntity = _mapper.Map<User>(user);
            if (_context.IsUserExists(user.UserName))
                return BadRequest("User already exists!");

            try
            {
                string token = _context.Register(userEntity, user.Password);
                _context.Save();
                return Ok(token);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("login")]
        public ActionResult<string> Login([FromBody] LoginDto user)
        {
            try
            {
                string token = _context.Login(user.UserName, user.Password);
                return Ok(token);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
