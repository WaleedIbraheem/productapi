﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Products.API.Entities;
using Products.API.Models;
using Products.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace Products.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductLibraryRepository _context;
        private readonly IMapper _mapper;

        public ProductsController(IProductLibraryRepository context, IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProductResultDto>> GetProducts([FromQuery] Guid? categoryId)
        {
            var productsFromRepo = _context.GetProducts(categoryId);
            if (productsFromRepo.Count() == 0)
                return StatusCode(StatusCodes.Status404NotFound, new { success = false, messages = "No Products Found" });
            var productsDto = _mapper.Map<IEnumerable<ProductResultDto>>(productsFromRepo);
            return StatusCode(StatusCodes.Status200OK, new { success = true, results = productsDto });
        }

        [HttpGet("{productId}", Name = nameof(GetProduct))]
        public ActionResult<ProductResultDto> GetProduct(Guid productId)
        {
            var productFromRepo = _context.GetProduct(productId);
            if (productFromRepo == null)
                return StatusCode(StatusCodes.Status404NotFound, new { success = false, messages = $"Product '{productId}' Not Found" });
            var productDto = _mapper.Map<ProductResultDto>(productFromRepo);
            return StatusCode(StatusCodes.Status200OK, new { success = true, results = productDto });
        }

        [Authorize]
        [HttpPost]
        public ActionResult<ProductResultDto> AddProduct(ProductDto product)
        {
            var userId = User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if (!_context.IsUserActive(new Guid(userId)))
                return StatusCode(StatusCodes.Status401Unauthorized, new { success = false, messages = "User not authorized!" });

            var productEntity = _mapper.Map<Product>(product);
            _context.AddProduct(productEntity);
            _context.Save();

            var productDto = _mapper.Map<ProductResultDto>(productEntity);
            return CreatedAtRoute(nameof(GetProduct), new { productId = productDto.ProductId }, new { success = true, results = productDto });
        }

        [Authorize]
        [HttpPut("{productId}")]
        public IActionResult UpdateProduct(Guid productId, ProductDto product)
        {
            var userId = User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if (!_context.IsUserActive(new Guid(userId)))
                return StatusCode(StatusCodes.Status401Unauthorized, new { success = false, messages = "User not authorized!" });

            var productFromRepo = _context.GetProduct(productId);
            if (productFromRepo == null)
                return StatusCode(StatusCodes.Status404NotFound, new { success = false, messages = $"Product '{productId}' Not Found" });

            _mapper.Map(product, productFromRepo);
            _context.UpdateProduct(productFromRepo);
            _context.Save();

            return NoContent();
        }

        [Authorize]
        [HttpDelete("{productId}")]
        public IActionResult DeleteProduct(Guid productId)
        {
            var userId = User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if (!_context.IsUserActive(new Guid(userId)))
                return StatusCode(StatusCodes.Status401Unauthorized, new { success = false, messages = "User not authorized!" });

            var productFromRepo = _context.GetProduct(productId);
            if (productFromRepo == null)
                return StatusCode(StatusCodes.Status404NotFound, new { success = false, messages = $"Product '{productId}' Not Found" });

            _context.DeleteProduct(productFromRepo);
            _context.Save();
            return NoContent();
        }

        [Authorize]
        [HttpPatch("{productId}")]
        public ActionResult PartiallyUpdateProduct(Guid productId, JsonPatchDocument<ProductDto> patchDocument)
        {
            var userId = User.FindFirst(c => c.Type == ClaimTypes.NameIdentifier).Value;
            if (!_context.IsUserActive(new Guid(userId)))
                return StatusCode(StatusCodes.Status401Unauthorized, new { success = false, messages = "User not authorized!" });

            var producrFromRepo = _context.GetProduct(productId);
            if (producrFromRepo == null)
                return StatusCode(StatusCodes.Status404NotFound, new { success = false, messages = $"Product '{productId}' Not Found" });

            var productToPatch = _mapper.Map<ProductDto>(producrFromRepo);
            patchDocument.ApplyTo(productToPatch, ModelState);

            if (!TryValidateModel(productToPatch))
                return ValidationProblem(ModelState);

            _mapper.Map(productToPatch, producrFromRepo);
            _context.UpdateProduct(producrFromRepo);
            _context.Save();

            return NoContent();
        }
    }
}
