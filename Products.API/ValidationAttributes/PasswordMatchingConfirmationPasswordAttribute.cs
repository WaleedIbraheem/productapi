﻿using Products.API.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.ValidationAttributes
{
    public class PasswordMatchingConfirmationPasswordAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var user = (RegisterDto)validationContext.ObjectInstance;
            if(user.Password != user.ConfirmPassword)
                return new ValidationResult(ErrorMessage, new[] { nameof(RegisterDto) });

            return ValidationResult.Success;
        }
    }
}
