﻿using Products.API.Models;
using Products.API.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.ValidationAttributes
{
    public class CategoryIdShouldBeExistsAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var _context = (IProductLibraryRepository)validationContext.GetService(typeof(IProductLibraryRepository));
            var product = (ProductDto)validationContext.ObjectInstance;
            if(product.CategoryId == null || product.CategoryId == Guid.Empty)
                return new ValidationResult(ErrorMessage, new[] { nameof(ProductDto) });
            
            if(!_context.IsCategoryExists(product.CategoryId))
                return new ValidationResult(ErrorMessage, new[] { nameof(ProductDto) });

            return ValidationResult.Success;
        }
    }
}
