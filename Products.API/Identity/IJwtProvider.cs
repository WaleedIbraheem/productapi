﻿using Products.API.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Identity
{
    public interface IJwtProvider
    {
        string GenerateToken(User user);
    }
}
