﻿using Products.API.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Services
{
    public interface IProductLibraryRepository
    {
        bool IsUserExists(string userName);
        bool IsUserActive(Guid userId);
        string Register(User user, string password);
        string Login(string userName, string password);
        IEnumerable<Category> GetCategories();
        bool IsCategoryExists(Guid categoryId);
        IEnumerable<Product> GetProducts(Guid? categoryId = null);
        Product GetProduct(Guid productId);
        void AddProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Product product);
        bool Save();
    }
}
