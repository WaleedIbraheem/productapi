﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Products.API.Entities;
using Products.API.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Products.API.Services
{
    public class ProductLibraryRepository : IProductLibraryRepository
    {
        private readonly ProductDbContext _context;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IJwtProvider _jwtProvider;

        public ProductLibraryRepository(ProductDbContext context, IPasswordHasher<User> passwordHasher, IJwtProvider jwtProvider)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _passwordHasher = passwordHasher ?? throw new ArgumentNullException(nameof(passwordHasher));
            _jwtProvider = jwtProvider ?? throw new ArgumentNullException(nameof(jwtProvider));
        }

        public bool IsUserExists(string userName)
        {
            if (userName == null)
                throw new ArgumentNullException(nameof(userName));

            return _context.Users.Any(u => u.UserName == userName);
        }
        public bool IsUserActive(Guid userId)
        {
            if (userId == null)
                throw new ArgumentNullException(nameof(userId));

            return _context.Users.Any(u => u.UserId == userId);
        }

        public string Register(User user, string password)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            if (IsUserExists(user.UserName))
                throw new Exception($"User {user.UserName} already exists!");

            user.UserId = Guid.NewGuid();
            string passwordHash = _passwordHasher.HashPassword(user, password);
            user.PasswordHash = passwordHash;
            _context.Users.Add(user);

            return _jwtProvider.GenerateToken(user);
        }

        public string Login(string userName, string password)
        {
            if (userName == null)
                throw new ArgumentNullException(nameof(userName));

            if (password == null)
                throw new ArgumentNullException(nameof(password));

            var user = _context.Users.Where(u => u.UserName == userName).FirstOrDefault();
            if (user == null)
                throw new Exception("Invalid UserName or Password!");

            var passwordVerified = _passwordHasher.VerifyHashedPassword(user, user.PasswordHash, password);
            if(passwordVerified == PasswordVerificationResult.Failed)
                throw new Exception("Invalid UserName or Password!");

            return _jwtProvider.GenerateToken(user);
        }

        public IEnumerable<Category> GetCategories()
        {
            return _context.Categories.ToList();
        }

        public bool IsCategoryExists(Guid categoryId)
        {
            if (categoryId == Guid.Empty)
                throw new ArgumentNullException(nameof(categoryId));

            return _context.Categories.Any(a => a.CategoryId == categoryId);
        }

        public IEnumerable<Product> GetProducts(Guid? categoryId = null)
        {
            if (categoryId != null)
                return _context.Products.Include(p => p.Category).Where(p => p.CategoryId == categoryId).ToList();
            
            return _context.Products.Include(p => p.Category).ToList();
        }

        public Product GetProduct(Guid productId)
        {
            if (productId == Guid.Empty)
                throw new ArgumentNullException(nameof(productId));
            return _context.Products.Include(p => p.Category).FirstOrDefault(p => p.ProductId == productId);
        }

        public void AddProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            product.ProductId = Guid.NewGuid();
            _context.Products.Add(product);
        }

        public void UpdateProduct(Product product)
        {
            // No Implementation
        }

        public void DeleteProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product));

            _context.Products.Remove(product);
        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }        
    }
}
