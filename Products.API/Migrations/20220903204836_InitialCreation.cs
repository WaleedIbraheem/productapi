﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Products.API.Migrations
{
    public partial class InitialCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    CategoryId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 30, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.CategoryId);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    ProductId = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    ImageUrl = table.Column<string>(nullable: true),
                    CategoryId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductId);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "CategoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "CategoryId", "Name" },
                values: new object[,]
                {
                    { new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"), "Dogi (Jackets & Pants)" },
                    { new Guid("618401de-da4a-4e74-83ef-dbef3e7f261d"), "Obi (Belts)" },
                    { new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"), "Hakama" },
                    { new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"), "Weapons" },
                    { new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "Books" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "ProductId", "CategoryId", "ImageUrl", "Name", "Price", "Quantity" },
                values: new object[,]
                {
                    { new Guid("2728fa87-823e-4dfe-9427-af58d39efb7f"), new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"), "https://cdn.shopify.com/s/files/1/0202/8734/products/KS100_01_300x300_crop_center.progressive.jpg?v=1649645254", "Pant", 40m, 88 },
                    { new Guid("2bbb5b1c-5df1-4f37-a556-0cd3939696f7"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/31IXlJgW6vL._BO1,204,203,200_.jpg", "Takemusu Aikido Vol 5", 200m, 3 },
                    { new Guid("ca88044e-f861-442d-815a-fcb902c5dd1c"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://imgproxy.pdfroom.com/3R_esQxZKSFzkQ_zlmIjcX1cDQN24XNCxGkT4dn4RD0/rs:auto:200:276:0/g:no/M3dXNW1sNlAyWW8uanBn.jpg", "Takemusu Aikido Vol 4", 200m, 6 },
                    { new Guid("a8c58f46-646e-47ad-9d17-43000b4766a5"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/51ENRTF1Q9L._SX348_BO1,204,203,200_.jpg", "Takemusu Aikido Vol 3", 200m, 6 },
                    { new Guid("ab20505b-df1d-4fa3-9b9d-adff15549d96"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/51c3I65J6yL.jpg", "Takemusu Aikido Vol 2", 200m, 3 },
                    { new Guid("b8dc788d-cbe0-4344-9c10-73407fbd6a75"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/71seVAxsggL.jpg", "Takemusu Aikido Vol 1", 200m, 8 },
                    { new Guid("4abf2e8b-8b2b-4b9a-a58c-ed3b5787ab2d"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://i.ebayimg.com/images/g/iocAAOSwdQdgY1MY/s-l500.jpg", "Traditional Aikido Complete Set", 800m, 3 },
                    { new Guid("468da13b-e9be-4357-9ca6-8863397a56f1"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://pictures.abebooks.com/isbn/9780870403729-uk.jpg", "Traditional Aikido Vol 5", 200m, 7 },
                    { new Guid("2447a3b3-053d-4886-9366-8dfff3150260"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://ia800802.us.archive.org/BookReader/BookReaderImages.php?zip=/6/items/TraditionalAikidoSwordStickBodyArtsVolume4VitalTechniquesByMorihiroSaito/Traditional%20Aikido%20-%20Sword%2C%20Stick%2C%20Body%20Arts%2C%20Volume%204%2C%20Vital%20Techniques%20by%20Morihiro%20Saito_jp2.zip&file=Traditional%20Aikido%20-%20Sword%2C%20Stick%2C%20Body%20Arts%2C%20Volume%204%2C%20Vital%20Techniques%20by%20Morihiro%20Saito_jp2/Traditional%20Aikido%20-%20Sword%2C%20Stick%2C%20Body%20Arts%2C%20Volume%204%2C%20Vital%20Techniques%20by%20Morihiro%20Saito_0000.jp2&id=TraditionalAikidoSwordStickBodyArtsVolume4VitalTechniquesByMorihiroSaito&scale=4&rotate=0", "Traditional Aikido Vol 4", 200m, 9 },
                    { new Guid("743bf241-14bf-40c3-b10f-1ef093eca4d1"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/51Y4kMCNy9L.jpg", "Traditional Aikido Vol 3", 200m, 7 },
                    { new Guid("4e49fb98-2ce6-4f88-a41d-646331722df6"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/51YhFOzwoeL.jpg", "Traditional Aikido Vol 2", 200m, 2 },
                    { new Guid("fb78bdca-b3fe-4948-9278-f607382b6434"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://images-na.ssl-images-amazon.com/images/I/81MFFn0W2YL.jpg", "Traditional Aikido Vol 1", 200m, 7 },
                    { new Guid("37fb50c1-2651-4a83-9f15-833162a4160c"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://m.media-amazon.com/images/I/41eEj9o17SL.jpg", "Takemusu Aikido Vol 6", 200m, 9 },
                    { new Guid("26f5be72-5d32-45bf-9b98-03aa57e14a31"), new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"), "https://cdn.shopify.com/s/files/1/0202/8734/products/Start-Set-01-en_400x400.progressive.jpg?v=1567385877", "Weapons Set", 190m, 12 },
                    { new Guid("e5ae74ee-36cd-410d-a5c4-e4142a1ba1c2"), new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"), "https://cdn.shopify.com/s/files/1/0202/8734/products/DSC2475_1_1_300x300_crop_center.progressive.jpg?v=1433736316", "Tanto", 20m, 20 },
                    { new Guid("c5c723f0-5329-40b1-b10c-a44181922419"), new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"), "https://cdn.shopify.com/s/files/1/0202/8734/products/bokken-standard-01-en_300x300_crop_center.progressive.jpg?v=1625119527", "Bokken", 90m, 16 },
                    { new Guid("b3f05216-b05e-42c1-901b-0acc5dd69428"), new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"), "https://cdn.shopify.com/s/files/1/0202/8734/products/DeluxeHanbo-01_en_300x300_crop_center.progressive.jpg?v=1606375695", "Jo", 110m, 12 },
                    { new Guid("27547a0f-09fe-44cd-8499-b35018ecbe87"), new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"), "https://cdn.shopify.com/s/files/1/0202/8734/products/nobakama-aikido-tetron-03_400x400.progressive.jpg?v=1568680143", "Aikido Nobakama (Navy)", 170m, 13 },
                    { new Guid("06d82e8c-c97a-469e-b71e-881c63d50caa"), new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"), "https://cdn.shopify.com/s/files/1/0202/8734/products/aikido-nobakama-tetron-3_large_8d6c75fd-700b-4346-9afa-08da5ee61ab1_400x400.progressive.jpg?v=1493189272", "Aikido Nobakama (Black)", 170m, 38 },
                    { new Guid("b8ff17ca-f42b-4482-a9b1-f27082f0f19d"), new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"), "https://cdn.shopify.com/s/files/1/0202/8734/products/AH450-02_400x400.progressive.jpg?v=1555491826", "Aikido Classic Hakama (Navy)", 135m, 10 },
                    { new Guid("3956e7f5-be86-4290-89a9-4e425d410e8d"), new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"), "https://cdn.shopify.com/s/files/1/0202/8734/products/AH450-01_300x300_crop_center.progressive.jpg?v=1555491826", "Aikido Classic Hakama (Black)", 135m, 40 },
                    { new Guid("ddfb2787-36c2-478e-87f1-8617e8cd4595"), new Guid("618401de-da4a-4e74-83ef-dbef3e7f261d"), "https://cdn.shopify.com/s/files/1/0202/8734/products/P1270409_300x300_crop_center.progressive.JPG?v=1539761234", "Black Belt", 45m, 50 },
                    { new Guid("2e8123a5-657a-4710-87a9-78ae7554a789"), new Guid("618401de-da4a-4e74-83ef-dbef3e7f261d"), "https://cdn.shopify.com/s/files/1/0202/8734/products/P1270464_300x300_crop_center.progressive.JPG?v=1535334018", "White Belt", 15m, 50 },
                    { new Guid("27f9bfa9-375a-4033-b842-b1ed1cf89545"), new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"), "https://cdn.shopify.com/s/files/1/0202/8734/products/KS100-01_17921908-3817-45dd-b3ca-2faac4f651c8.progressive.jpg?v=1649318260", "Jacket & Pant", 110m, 12 },
                    { new Guid("234680c1-6aa2-42a6-8384-4ea2edf45a1c"), new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"), "https://cdn.shopify.com/s/files/1/0202/8734/products/AS200-04_346f2478-a5f9-45c3-9e89-c7d4e9ae9876_400x400.progressive.jpg?v=1649298433", "Jacket", 75m, 70 },
                    { new Guid("1a8dfad0-c0d7-4f28-8a9b-e80932057eec"), new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"), "https://cdn.shopify.com/s/files/1/0202/8734/products/Linen-bag-01_300x300_crop_center.progressive.jpg?v=1630476244", "Weapons Bag", 15m, 40 },
                    { new Guid("cdf3f9af-0edf-4b7b-9f08-147f74ed73e4"), new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"), "https://vhx.imgix.net/aikidojournal/assets/83fd5b60-26c8-43fb-8fd3-f3884919457f.png?auto=format%2Ccompress&fit=crop&h=720&w=1280", "Takemusu Aikido Vol Complete Set", 900m, 3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
