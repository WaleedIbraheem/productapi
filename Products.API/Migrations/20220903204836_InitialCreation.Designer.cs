﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Products.API.Entities;

namespace Products.API.Migrations
{
    [DbContext(typeof(ProductDbContext))]
    [Migration("20220903204836_InitialCreation")]
    partial class InitialCreation
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Products.API.Entities.Category", b =>
                {
                    b.Property<Guid>("CategoryId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(30)")
                        .HasMaxLength(30);

                    b.HasKey("CategoryId");

                    b.ToTable("Categories");

                    b.HasData(
                        new
                        {
                            CategoryId = new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"),
                            Name = "Dogi (Jackets & Pants)"
                        },
                        new
                        {
                            CategoryId = new Guid("618401de-da4a-4e74-83ef-dbef3e7f261d"),
                            Name = "Obi (Belts)"
                        },
                        new
                        {
                            CategoryId = new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"),
                            Name = "Hakama"
                        },
                        new
                        {
                            CategoryId = new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"),
                            Name = "Weapons"
                        },
                        new
                        {
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            Name = "Books"
                        });
                });

            modelBuilder.Entity("Products.API.Entities.Product", b =>
                {
                    b.Property<Guid>("ProductId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("CategoryId")
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("ImageUrl")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("nvarchar(50)")
                        .HasMaxLength(50);

                    b.Property<decimal>("Price")
                        .HasColumnType("decimal(18,2)");

                    b.Property<int>("Quantity")
                        .HasColumnType("int");

                    b.HasKey("ProductId");

                    b.HasIndex("CategoryId");

                    b.ToTable("Products");

                    b.HasData(
                        new
                        {
                            ProductId = new Guid("2728fa87-823e-4dfe-9427-af58d39efb7f"),
                            CategoryId = new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/KS100_01_300x300_crop_center.progressive.jpg?v=1649645254",
                            Name = "Pant",
                            Price = 40m,
                            Quantity = 88
                        },
                        new
                        {
                            ProductId = new Guid("234680c1-6aa2-42a6-8384-4ea2edf45a1c"),
                            CategoryId = new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/AS200-04_346f2478-a5f9-45c3-9e89-c7d4e9ae9876_400x400.progressive.jpg?v=1649298433",
                            Name = "Jacket",
                            Price = 75m,
                            Quantity = 70
                        },
                        new
                        {
                            ProductId = new Guid("27f9bfa9-375a-4033-b842-b1ed1cf89545"),
                            CategoryId = new Guid("78cf02c6-d368-4d83-bfac-ca3cc3ec0fb5"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/KS100-01_17921908-3817-45dd-b3ca-2faac4f651c8.progressive.jpg?v=1649318260",
                            Name = "Jacket & Pant",
                            Price = 110m,
                            Quantity = 12
                        },
                        new
                        {
                            ProductId = new Guid("2e8123a5-657a-4710-87a9-78ae7554a789"),
                            CategoryId = new Guid("618401de-da4a-4e74-83ef-dbef3e7f261d"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/P1270464_300x300_crop_center.progressive.JPG?v=1535334018",
                            Name = "White Belt",
                            Price = 15m,
                            Quantity = 50
                        },
                        new
                        {
                            ProductId = new Guid("ddfb2787-36c2-478e-87f1-8617e8cd4595"),
                            CategoryId = new Guid("618401de-da4a-4e74-83ef-dbef3e7f261d"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/P1270409_300x300_crop_center.progressive.JPG?v=1539761234",
                            Name = "Black Belt",
                            Price = 45m,
                            Quantity = 50
                        },
                        new
                        {
                            ProductId = new Guid("3956e7f5-be86-4290-89a9-4e425d410e8d"),
                            CategoryId = new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/AH450-01_300x300_crop_center.progressive.jpg?v=1555491826",
                            Name = "Aikido Classic Hakama (Black)",
                            Price = 135m,
                            Quantity = 40
                        },
                        new
                        {
                            ProductId = new Guid("b8ff17ca-f42b-4482-a9b1-f27082f0f19d"),
                            CategoryId = new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/AH450-02_400x400.progressive.jpg?v=1555491826",
                            Name = "Aikido Classic Hakama (Navy)",
                            Price = 135m,
                            Quantity = 10
                        },
                        new
                        {
                            ProductId = new Guid("06d82e8c-c97a-469e-b71e-881c63d50caa"),
                            CategoryId = new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/aikido-nobakama-tetron-3_large_8d6c75fd-700b-4346-9afa-08da5ee61ab1_400x400.progressive.jpg?v=1493189272",
                            Name = "Aikido Nobakama (Black)",
                            Price = 170m,
                            Quantity = 38
                        },
                        new
                        {
                            ProductId = new Guid("27547a0f-09fe-44cd-8499-b35018ecbe87"),
                            CategoryId = new Guid("ca833b41-9b40-40e6-93a7-9f85bccbcf07"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/nobakama-aikido-tetron-03_400x400.progressive.jpg?v=1568680143",
                            Name = "Aikido Nobakama (Navy)",
                            Price = 170m,
                            Quantity = 13
                        },
                        new
                        {
                            ProductId = new Guid("b3f05216-b05e-42c1-901b-0acc5dd69428"),
                            CategoryId = new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/DeluxeHanbo-01_en_300x300_crop_center.progressive.jpg?v=1606375695",
                            Name = "Jo",
                            Price = 110m,
                            Quantity = 12
                        },
                        new
                        {
                            ProductId = new Guid("c5c723f0-5329-40b1-b10c-a44181922419"),
                            CategoryId = new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/bokken-standard-01-en_300x300_crop_center.progressive.jpg?v=1625119527",
                            Name = "Bokken",
                            Price = 90m,
                            Quantity = 16
                        },
                        new
                        {
                            ProductId = new Guid("e5ae74ee-36cd-410d-a5c4-e4142a1ba1c2"),
                            CategoryId = new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/DSC2475_1_1_300x300_crop_center.progressive.jpg?v=1433736316",
                            Name = "Tanto",
                            Price = 20m,
                            Quantity = 20
                        },
                        new
                        {
                            ProductId = new Guid("1a8dfad0-c0d7-4f28-8a9b-e80932057eec"),
                            CategoryId = new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/Linen-bag-01_300x300_crop_center.progressive.jpg?v=1630476244",
                            Name = "Weapons Bag",
                            Price = 15m,
                            Quantity = 40
                        },
                        new
                        {
                            ProductId = new Guid("26f5be72-5d32-45bf-9b98-03aa57e14a31"),
                            CategoryId = new Guid("2a61ea0c-e0ac-4b40-bef1-3acb02e6d7b9"),
                            ImageUrl = "https://cdn.shopify.com/s/files/1/0202/8734/products/Start-Set-01-en_400x400.progressive.jpg?v=1567385877",
                            Name = "Weapons Set",
                            Price = 190m,
                            Quantity = 12
                        },
                        new
                        {
                            ProductId = new Guid("fb78bdca-b3fe-4948-9278-f607382b6434"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/81MFFn0W2YL.jpg",
                            Name = "Traditional Aikido Vol 1",
                            Price = 200m,
                            Quantity = 7
                        },
                        new
                        {
                            ProductId = new Guid("4e49fb98-2ce6-4f88-a41d-646331722df6"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/51YhFOzwoeL.jpg",
                            Name = "Traditional Aikido Vol 2",
                            Price = 200m,
                            Quantity = 2
                        },
                        new
                        {
                            ProductId = new Guid("743bf241-14bf-40c3-b10f-1ef093eca4d1"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/51Y4kMCNy9L.jpg",
                            Name = "Traditional Aikido Vol 3",
                            Price = 200m,
                            Quantity = 7
                        },
                        new
                        {
                            ProductId = new Guid("2447a3b3-053d-4886-9366-8dfff3150260"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://ia800802.us.archive.org/BookReader/BookReaderImages.php?zip=/6/items/TraditionalAikidoSwordStickBodyArtsVolume4VitalTechniquesByMorihiroSaito/Traditional%20Aikido%20-%20Sword%2C%20Stick%2C%20Body%20Arts%2C%20Volume%204%2C%20Vital%20Techniques%20by%20Morihiro%20Saito_jp2.zip&file=Traditional%20Aikido%20-%20Sword%2C%20Stick%2C%20Body%20Arts%2C%20Volume%204%2C%20Vital%20Techniques%20by%20Morihiro%20Saito_jp2/Traditional%20Aikido%20-%20Sword%2C%20Stick%2C%20Body%20Arts%2C%20Volume%204%2C%20Vital%20Techniques%20by%20Morihiro%20Saito_0000.jp2&id=TraditionalAikidoSwordStickBodyArtsVolume4VitalTechniquesByMorihiroSaito&scale=4&rotate=0",
                            Name = "Traditional Aikido Vol 4",
                            Price = 200m,
                            Quantity = 9
                        },
                        new
                        {
                            ProductId = new Guid("468da13b-e9be-4357-9ca6-8863397a56f1"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://pictures.abebooks.com/isbn/9780870403729-uk.jpg",
                            Name = "Traditional Aikido Vol 5",
                            Price = 200m,
                            Quantity = 7
                        },
                        new
                        {
                            ProductId = new Guid("4abf2e8b-8b2b-4b9a-a58c-ed3b5787ab2d"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://i.ebayimg.com/images/g/iocAAOSwdQdgY1MY/s-l500.jpg",
                            Name = "Traditional Aikido Complete Set",
                            Price = 800m,
                            Quantity = 3
                        },
                        new
                        {
                            ProductId = new Guid("b8dc788d-cbe0-4344-9c10-73407fbd6a75"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/71seVAxsggL.jpg",
                            Name = "Takemusu Aikido Vol 1",
                            Price = 200m,
                            Quantity = 8
                        },
                        new
                        {
                            ProductId = new Guid("ab20505b-df1d-4fa3-9b9d-adff15549d96"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/51c3I65J6yL.jpg",
                            Name = "Takemusu Aikido Vol 2",
                            Price = 200m,
                            Quantity = 3
                        },
                        new
                        {
                            ProductId = new Guid("a8c58f46-646e-47ad-9d17-43000b4766a5"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/51ENRTF1Q9L._SX348_BO1,204,203,200_.jpg",
                            Name = "Takemusu Aikido Vol 3",
                            Price = 200m,
                            Quantity = 6
                        },
                        new
                        {
                            ProductId = new Guid("ca88044e-f861-442d-815a-fcb902c5dd1c"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://imgproxy.pdfroom.com/3R_esQxZKSFzkQ_zlmIjcX1cDQN24XNCxGkT4dn4RD0/rs:auto:200:276:0/g:no/M3dXNW1sNlAyWW8uanBn.jpg",
                            Name = "Takemusu Aikido Vol 4",
                            Price = 200m,
                            Quantity = 6
                        },
                        new
                        {
                            ProductId = new Guid("2bbb5b1c-5df1-4f37-a556-0cd3939696f7"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://images-na.ssl-images-amazon.com/images/I/31IXlJgW6vL._BO1,204,203,200_.jpg",
                            Name = "Takemusu Aikido Vol 5",
                            Price = 200m,
                            Quantity = 3
                        },
                        new
                        {
                            ProductId = new Guid("37fb50c1-2651-4a83-9f15-833162a4160c"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://m.media-amazon.com/images/I/41eEj9o17SL.jpg",
                            Name = "Takemusu Aikido Vol 6",
                            Price = 200m,
                            Quantity = 9
                        },
                        new
                        {
                            ProductId = new Guid("cdf3f9af-0edf-4b7b-9f08-147f74ed73e4"),
                            CategoryId = new Guid("1ead2374-e6ee-42c9-b336-093ee8d13b8f"),
                            ImageUrl = "https://vhx.imgix.net/aikidojournal/assets/83fd5b60-26c8-43fb-8fd3-f3884919457f.png?auto=format%2Ccompress&fit=crop&h=720&w=1280",
                            Name = "Takemusu Aikido Vol Complete Set",
                            Price = 900m,
                            Quantity = 3
                        });
                });

            modelBuilder.Entity("Products.API.Entities.Product", b =>
                {
                    b.HasOne("Products.API.Entities.Category", "Category")
                        .WithMany()
                        .HasForeignKey("CategoryId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
