﻿using Products.API.ValidationAttributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Models
{
    [CategoryIdShouldBeExistsAttribute(ErrorMessage = "CategoryId is invalid!")]
    public class ProductDto
    {
        [Required(ErrorMessage = "Require attribute 'Name' is missing!")]
        [MaxLength(50, ErrorMessage = "Name cannot exceed 50 character!")]
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public int Quantity { get; set; }
        
        public string ImageUrl { get; set; }
        
        [Required(ErrorMessage = "Require attribute 'CategoryId' is missing!")]
        public Guid CategoryId { get; set; }
    }
}
