﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Models
{
    public class ProductResultDto : ProductDto
    {
        public Guid ProductId { get; set; }
        public string CategoryName { get; set; }

    }
}
