﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Models
{
    public class CategoryResultDto
    {
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
    }
}
