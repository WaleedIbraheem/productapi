﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Entities
{
    public class Category
    {
        [Key]
        public Guid CategoryId { get; set; }
        
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }
    }
}
