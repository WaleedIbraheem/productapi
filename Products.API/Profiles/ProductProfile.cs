﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Products.API.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<Entities.Product, Models.ProductResultDto>()
                .ForMember(p => p.CategoryName, map => map.MapFrom(p => p.Category.Name));
            CreateMap<Models.ProductDto, Entities.Product>().ReverseMap();
        }
    }
}
